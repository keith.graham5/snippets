import pandas as pd

def _process_omim_gene_map_data(omingenemap_filepath):
    """ Process a genes inheritance data file from Omim
    """
    chunked_data = pd.read_csv(omingenemap_filepath,
                               skiprows=3,
                               sep='\t',
                               lineterminator='\n',
                               header=None)
    # Create df from txt_file
    df = pd.DataFrame(chunked_data)
    # Replace the null values with specified value
    # df.fillna('', inplace=True)
    # Store contents of df row 0 as new_header
    new_header =df.iloc[0]
    # Create df from row 1 of previous df, skips row zero
    df = df[1:]
    # Assign new_headers to df
    df.columns = new_header



    # Create empty list for inheritance_patterns
    inheritance_patterns = []
    # iterate through specified row of df
    for row in df["Phenotypes"]:
        # Create empty list ready for the inheritance patterns for each entry i.e. row
        ips_in_index = []
        # Convert index contents to string
        pheno_description = str(row)
        if "Autosomal dominant" in pheno_description:
            ips_in_index.append("AD")
        if "Autosomal recessive" in pheno_description:
            ips_in_index.append("AR")
        if "X-linked recessive" in pheno_description:
            ips_in_index.append("XLR")
        if "X-linked dominant" in pheno_description:
            ips_in_index.append("XLD")
        if "Y-linked" in pheno_description:
            ips_in_index.append("YL")
        if "Mitochondrial" in pheno_description:
            ips_in_index.append("Mito")
        if not ips_in_index:
            ips_in_index.append("NF")
        # append each list into list
        inheritance_patterns.append(ips_in_index)
    # Create new column in df and  insert contents
    df["Gene Inheritance Patterns"] = inheritance_patterns
    print(df.loc[121])
    print(df.iloc[121][0])


_process_omim_gene_map_data("/home/keith/Data/Omimgenemap2.txt")